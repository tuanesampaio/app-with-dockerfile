package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_PrintHello(t *testing.T){
	req, err := http.NewRequest ("GET", "http://example.com/foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()
	PrintHello(res, req)

	exp := "Hello, "
	act := res.Body.String()
	if exp != act {
		t.Fatalf("Expected %s but return %s", exp, act)
	}
}
