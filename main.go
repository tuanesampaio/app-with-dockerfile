package main

import (
	"fmt"
	"html"
	"net/http"
)

func PrintHello(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}


func main(){

	http.HandleFunc("/", PrintHello)
	http.ListenAndServe(":8080", nil)

}